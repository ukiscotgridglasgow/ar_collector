#! /bin/bash

month=`date +%m`
year=`date +%Y`
day="01"

site="UKI-SCOTGRID-GLASGOW"
vo=atlas

while getopts "hy:m:s:" o; do
    case "${o}" in
        h)
            echo Usage
            ;;

        y)
            y=${OPTARG}
            yeardate="Jan-01-"$y
            year=`date -d "$yeardate" +"%Y"`
            ;;
        m)
            m=${OPTARG}
            monthdate=$m"-01-2015"
            month=`date -d "$monthdate" +%m`
            ;;
        s)
            site=${OPTARG}
            ;;
        *)
            echo Usage
            ;;
    esac
done

startdate=`date -d "$year$month$day" +"%d-%b-%Y"`
enddate=`date -d "$year$month$day + 1 month - 1 day" +"%d-%b-%Y"`

startstamp=`date -d "12:00 $year$month$day" +%s`
endstamp=`date -d "12:00 $year$month$day + 1 month - 1 day" +%s`

url="http://wlcg-sam.cern.ch/dashboard/request.py/reportsGeneration?report=all&vo=$vo&start_time=$startdate&end_time=$enddate&type=html&profile=ATLAS_CRITICAL"

output=`curl -s $url | grep -A 1 $site | grep -v $site | cut -d\> -f2 | cut -d% -f1`

echo "metric.wlcg.availability.$vo.$site $output $startstamp"  | socat STDIN TCP:CARBONSERVER:CARBONPORT
