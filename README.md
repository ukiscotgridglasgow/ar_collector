# ar_collector.sh

This script generates the availability data for a given site from the WLCG SAM report generator. It assumes the use of [graphite](https://graphiteapp.org) and requires `socat` on the host node. The accompanying [grafana](https://grafana.com) dashboard, `ar.json`, displays the data. In the first instance it is configured for UKI-SCOTGRID-GLASGOW/ATLAS, but should be flexible.

# ar.json

This is a grafana dashboard that uses a graphite datasource to populate a dashboard showing availability for a given site (by default UKI-SCOTGRID-GLASGOW/ATLAS). It assumes a metric namespace of the form

- metric.wlcg.availability.VO\_NAME.SITE\_NAME

# wlcg_agis [storage-schema.conf]

This snippet is an example of how often you might poll this data; for insertion into a carbon storage-schema.conf file.